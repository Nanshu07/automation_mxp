package Page_Methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import PageObjects.LoginPage;
import DataProviders.LoginDataProvider;

public class LoginMethods {
	LoginPage login;
	
	public LoginMethods(WebDriver driver) {
	
		login= PageFactory.initElements(driver, LoginPage.class);}
  @Test(dataProvider = "loginCredentials", dataProviderClass = LoginDataProvider.class)
  public void withValidCredentials(String Username, String Password){
	 login.getUsername().sendKeys(Username); 
	login.getPassword().sendKeys(Password);
	login.getloginBtn().click();
  }
}
