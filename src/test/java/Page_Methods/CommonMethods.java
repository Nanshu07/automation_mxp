package Page_Methods;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;



import BaseTest.BaseClass;
import PageObjects.WebHomePageObjects;

public class CommonMethods extends BaseClass {
	WebHomePageObjects homepage;
	WebDriver driver;
	 public String actualTitle;
	public CommonMethods(WebDriver driver) {
		this.driver = driver;
		homepage = PageFactory.initElements(driver, WebHomePageObjects.class);
		homepage=new WebHomePageObjects();
		
	}
	
	public String takeScreenShot(WebElement element, String fileName) throws IOException {
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg = ImageIO.read(screenshot);
		Point point = element.getLocation();
		int eleWidth = element.getSize().getWidth();
		int eleHeight = element.getSize().getHeight();
		BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);
		FileUtils.copyFile(screenshot, new File("D:\\" + fileName + ".png"));
		return "D:\\" + fileName + ".png";

	}
	public String takeScreenShotLists(List<WebElement> list, String fileName) throws IOException {
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		BufferedImage fullImg = ImageIO.read(screenshot);
		Point point = ((WebElement) list).getLocation();
		int eleWidth = ((WebElement) list).getSize().getWidth();
		int eleHeight = ((WebElement) list).getSize().getHeight();
		BufferedImage eleScreenshot = fullImg.getSubimage(point.getX(), point.getY(), eleWidth, eleHeight);
		ImageIO.write(eleScreenshot, "png", screenshot);
		/*FileUtils.copyFile(screenshot, new File(System.getProperty("user.home") + "/screenshots/" + fileName + ".png"));
		return System.getProperty("user.home") + "/screenshots/" + fileName + ".png";*/
		FileUtils.copyFile(screenshot, new File("D:\\" + fileName + ".png"));
		return "D:\\" + fileName + ".png";

	}
	
}
