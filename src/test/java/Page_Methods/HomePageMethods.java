package Page_Methods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;


import PageObjects.CmsHome;


public class HomePageMethods {

	CmsHome home;
	WebDriver driver;
	private String homePageTitle = "MensXP";
	private String actualTitle = driver.getTitle();

	public HomePageMethods(WebDriver driver) {
		this.driver = driver;
		home = PageFactory.initElements(driver, CmsHome.class);
	}

	@BeforeTest
	public void VerifyTitle() {
		Assert.assertEquals(homePageTitle, actualTitle);
	}

	public void ClickOriginalContent() {

		Actions action = new Actions(driver);

		action.moveToElement(home.getMenuItem()).moveToElement(home.getSubMenuItem()).click();
		

	}

}
