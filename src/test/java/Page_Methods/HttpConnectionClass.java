package Page_Methods;

import java.io.IOException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.ProtocolException;




public class HttpConnectionClass {
	
	public int isLinkBroken(String href) throws Exception
	 
	{
		String userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36";
		URL url = null;
		
		try {
		url = new URL(href);
		} catch (MalformedURLException e) {
		System.out.println("Failed while checking for ==> " + href);
		e.printStackTrace();
		}
		HttpURLConnection http = null;
		try {
		http = (HttpURLConnection) url.openConnection();
		} catch (IOException e1) {
		System.out.println("Failed while checking for ==> " + href);
		e1.printStackTrace();
		}
		try {
		http.setRequestMethod("GET");
		} catch (ProtocolException e) {
		System.out.println("Failed while checking for ==> " + href);
		e.printStackTrace();
		}
		http.addRequestProperty("User-Agent", userAgent);
		try {
		http.connect();
		} catch (IOException e) {
		System.out.println("Failed while checking for ==> " + href);
		e.printStackTrace();
		}
		int responseCode = 0;
		try {
		responseCode = http.getResponseCode();
		System.out.println("responsecode was" +responseCode);
		} catch (IOException e) {
		System.out.println("Failed while checking for ==> " + href);
		e.printStackTrace();
		}
		return responseCode;
		} 
	
	
}