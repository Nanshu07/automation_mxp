package TestCases;

import java.net.MalformedURLException;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import BaseTest.BaseClass;
import DataProviders.SearchContentProvider;
import PageObjects.WebHomePageObjects;
import Page_Methods.HttpConnectionClass;
import Page_Methods.WebHomePageMethods;

public class WebHomePageHeaderTest extends BaseClass{
	BaseClass b2;
	WebHomePageMethods wh1;
	WebHomePageObjects homepageobjct;
	HttpConnectionClass con;
	

	String apUrl = "http://www.mensxp.com/";
	@BeforeClass(alwaysRun = true)
	public void alwayssetup() {
		b2 = new BaseClass();
		homepageobjct = PageFactory.initElements(driver, WebHomePageObjects.class);
		homepageobjct = new WebHomePageObjects();
		wh1 = new WebHomePageMethods(driver);
		con = new HttpConnectionClass();
	}
	@Test
	@Parameters("browserType")
	public void GotoHomePage() {

		b2.initializeTestBaseSetup("chrome", apUrl);
	}

	@Test(description = "verify Test Case Title", priority = 0)
	public void verifyTitle() throws Throwable {

		AssertJUnit.assertEquals(homepageobjct.getACtualTitle(driver), homepageobjct.expectedTitle);

	}
	
	@Test(description="verify HealthMenuDetails",priority=1)
	public void verifyHeader(){
		Actions action=new Actions(driver);
				
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		action.moveToElement(homepageobjct.getHeaderHealth1(driver)).build().perform();;
		
	    driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	  WebDriverWait wait=new WebDriverWait(driver, 25);
		System.out.println(homepageobjct.getHeaderHealth(driver).size());
		for (WebElement e : homepageobjct.getHeaderHealth(driver)) {
			System.out.println(e.getText());
		wait.until(ExpectedConditions.visibilityOfAllElements(homepageobjct.getHealthSubmenu(driver)));
		System.out.println("Health menu was present");
		System.out.println();
		
	}}
	@Test(description="verify relationship header",priority=2)
		public void verifyRelationshipHeader(){
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		Actions action=new Actions(driver);
		
		action.moveToElement(homepageobjct.getheaderRelatnshp(driver)).build().perform();
		
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    
		System.out.println(homepageobjct.getRElationshipmenu(driver).size());
		for (WebElement e : homepageobjct.getRElationshipmenu(driver)) {
		System.out.println(e.getText());
		System.out.println("Relationship menu was present......................");
		}
	}
	@Test(description="verify Fashion header",priority=3)
	public void verifyFashionHeader(){
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	Actions action=new Actions(driver);
	action.moveToElement(homepageobjct.getFashionHeader(driver)).build().perform();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	System.out.println(homepageobjct.getFashionMenu(driver).size());
	for (WebElement e : homepageobjct.getFashionMenu(driver)) {
		System.out.println(e.getText());
		System.out.println("Fashion  menu was present.......................");
}
	}
	@Test(description="verify Grooming header",priority=4)
	public void verifyGroomingHeader(){
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	Actions action=new Actions(driver);
	action.moveToElement(homepageobjct.getGroomingHeader(driver)).build().perform();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	System.out.println(homepageobjct.getGroomingMenu(driver).size());
	for (WebElement e : homepageobjct.getGroomingMenu(driver)) {
		System.out.println(e.getText());
		System.out.println("Grooming  menu was present.............");
}
	}
	@Test(description="verify WorkLife header",priority=5)
	public void verifyWorklifeHeader(){
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	Actions action=new Actions(driver);
	action.moveToElement(homepageobjct.getWorkLifeHeader(driver)).build().perform();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	System.out.println(homepageobjct.getWorkLifeMenu(driver).size());
	for (WebElement e : homepageobjct.getWorkLifeMenu(driver)) {
		System.out.println(e.getText());
		System.out.println("Worklife  menu was present........");
}}
	@Test(description="verify Technology header",priority=6)
	public void verifyTechnologyHeader(){
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	Actions action=new Actions(driver);
	action.moveToElement(homepageobjct.getTechnologyHeader(driver)).build().perform();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	System.out.println(homepageobjct.getTechnologyMenu(driver).size());
	for (WebElement e : homepageobjct.getTechnologyMenu(driver)) {
		System.out.println(e.getText());
		System.out.println("Technology  menu was present...........");
}
	}
	
	@Test(description="verify Entertainment header",priority=7)
	public void verifyEntertainmentSHeader(){
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	Actions action=new Actions(driver);
	action.moveToElement(homepageobjct.getEntertainmentHeader(driver)).build().perform();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	System.out.println(homepageobjct.getEntertainmentMenu(driver).size());
	for (WebElement e : homepageobjct.getEntertainmentMenu(driver)) {
		System.out.println(e.getText());
		System.out.println("Entertainment  menu was present..........");
}
	}
	@Test(description="Verify Search API",priority=8,dataProvider = "SearchContent", dataProviderClass = SearchContentProvider.class)
	public void VerifySearchAPI(String Url) throws MalformedURLException, Exception{
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		System.out.println("URL: " + Url + " returned "
				+ con.isLinkBroken(Url));
		

	}
}



