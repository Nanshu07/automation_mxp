package TestCases;


import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import BaseTest.BaseClass;
import PageObjects.WebHomePageObjects;
import Page_Methods.HttpConnectionClass;

public class WebHomePageVerifyLinks extends BaseClass {

	BaseClass b2;
	WebHomePageObjects homepageobjct;
	HttpConnectionClass con;

	String apUrl = "http://amp.mensxp.com/healthcheck";

	@BeforeClass(alwaysRun = true)
	public void alwayssetup() {
		b2 = new BaseClass();
		homepageobjct = PageFactory.initElements(driver, WebHomePageObjects.class);
		homepageobjct = new WebHomePageObjects();
		con = new HttpConnectionClass();
	}

	@Test
	@Parameters("browserType")
	public void GotoHomePage() {

		b2.initializeTestBaseSetup("chrome", apUrl);
	}
/*
	@Test
	public void printUrls(){
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		for (WebElement element : homepageobjct.getAllLinks(driver)) {
			System.out.println(element.getAttribute("href"));
		}
	}*/
	
	@Test
	public void VerifyUrlsStatus() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		for (WebElement element : homepageobjct.getAllLinks(driver)) {
		
			try

			{
				
				
				if (element != null && !element.getAttribute("href").contains("javascript") && element.getAttribute("href").contains("http")) 
				{
					System.out.println("URL: " + element.getAttribute("href") + " returned "
							+ con.isLinkBroken(element.getAttribute("href")));

				}
			}

			catch (Exception exp)

			{
				System.out.println(
						"At " + element.getAttribute("innerHTML") + " Exception occured -&gt; " + exp.getMessage());
			}
		}
	}
}
