package TestCases;

import org.testng.annotations.Test;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.AssertJUnit;
import org.testng.Reporter;

import java.io.File;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import BaseTest.BaseClass;
import PageObjects.WebHomePageObjects;
import Page_Methods.CommonMethods;
import Page_Methods.WebHomePageMethods;
import Reporter.WebAutomationReporter;
import Utilities.SendEmail;


@Listeners(WebAutomationReporter.class)

public class HomePageTestCases extends BaseClass {

	BaseClass b2;
	WebHomePageMethods wh1;
	WebHomePageObjects homepageobjct;
	CommonMethods common;
	SendEmail email;

	String apUrl = "http://www.mensxp.com/";
	String fromEmail="nanshutimes2016@gmail.com";
	String toEmail="nanshu.arora@timesinternet.in";
	String reportFileName="emailable-report.html";
	String SuiteName=" MensXP Homepage";

	@BeforeClass(alwaysRun = true)
	public void alwayssetup() {
b2 = new BaseClass();
		homepageobjct = PageFactory.initElements(driver, WebHomePageObjects.class);
		homepageobjct = new WebHomePageObjects();
		wh1 = new WebHomePageMethods(driver);
		common=new CommonMethods(driver);
		email=new SendEmail();
	}
	
	
@Test
@Parameters("browserType")
	public void GotoHomePage() {

		b2.initializeTestBaseSetup("chrome", apUrl);
	}

@Test(description = "verify Test Case Title", priority = 0)
	public void verifyTitle() throws Throwable {

		AssertJUnit.assertEquals(homepageobjct.getACtualTitle(driver), homepageobjct.expectedTitle);

	}

@Test(description = "verify menu Items", priority = 1)

	public void verifyMenuitems() throws Throwable {

		try {

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getmenuItems(driver).size());
			for (WebElement e : homepageobjct.getmenuItems(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getmenuItems(driver).size(), 0, "Menu Items are Blank");
				Assert.assertTrue(e.getText().contains("HEALTH"), "health is not present");
				Assert.assertTrue(e.getText().contains("RELATIONSHIPS"), "RELATIONSHIPS is not present");
				Assert.assertTrue(e.getText().contains("FASHION"), "FASHION is not present");
				Assert.assertTrue(e.getText().contains("GROOMING"), "GROOMING is not present");
				Assert.assertTrue(e.getText().contains("POWER & MONEY"), "POWER & MONEY is not present");
				Assert.assertTrue(e.getText().contains("TECHNOLOGY"), "TECHNOLOGY is not present");
				Assert.assertTrue(e.getText().contains("ENTERTAINMENT"), "ENTERTAINMENT is not present");

			}
		} catch (Exception e) {
			System.out.println("screenshot taken");
			common.takeScreenShotLists(homepageobjct.getmenuItems(driver), "VerifyMenuItems");
		}

	}

	@Test(description = "Verify Logo", priority = 2)
	public void verifyLogo() {
		wh1.clickLogo(driver);
		System.out.println("clicking logo");

		AssertJUnit.assertEquals(homepageobjct.getACtualTitle(driver), homepageobjct.expectedTitle);

	}

	@Test(description = "verify Header Ad", priority = 3)

	public void verifyHeaderAd() throws Exception {

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		if (homepageobjct.getAdFrame(driver).isDisplayed()) {
			System.out.println("Adframe is Visible");
		} else {

			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
			System.out.println("Element is InVisible");
		}
	}

	@Test(description = "verify Main Banners", priority = 4)
	public void verifyMainBanners() throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		int i = homepageobjct.getBannerData(driver).size();
		System.out.println(i);
		for (int k = 0; k <= i; k++) {
			Thread.sleep(1000);
			homepageobjct.getArrow(driver).click();
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}
	}

	@Test(description = "Verify Whats Hot section", priority = 6)
	
	private void verifyWHtsHotSection() throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getWhtsHotsection(driver).size());
			for (WebElement e : homepageobjct.getWhtsHotsection(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getWhtsHotsection(driver).size(), 0, "Menu Items are Blank");
			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}
	}

	@Test(description = "verify Trending Community section", priority = 7)
	public void verifyTrendingcom() throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getTrendingCom(driver).size());
			for (WebElement e : homepageobjct.getTrendingCom(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getTrendingCom(driver).size(), 0, "Menu Items are Blank");
			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}
	}

	@Test(description = "verify gallery section", priority = 7)
	public void verifyGallery() throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getGallery(driver).size());
			for (WebElement e : homepageobjct.getGallery(driver)) {
				System.out.println(e.getText());
			
				Assert.assertNotEquals(homepageobjct.getGallery(driver).size(), 0, "Menu Items are Blank");

			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}

	}

	@Test(description = "verify Technology section", priority = 7)
	public void verifyTechSection() throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getTechSection(driver).size());
			for (WebElement e : homepageobjct.getTechSection(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getTechSection(driver).size(), 0, "Menu Items are Blank");

			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}

	}

	@Test(description = "verify Relationship section", priority = 7)
	public void verifyRelationSection() throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getRelationSection(driver).size());
			for (WebElement e : homepageobjct.getRelationSection(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getRelationSection(driver).size(), 0, "Menu Items are Blank");

			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}

	}

	@Test(description = "verify Health section", priority = 7)
	public void verifyHealthSection() throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getHealthSection(driver).size());
			for (WebElement e : homepageobjct.getHealthSection(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getHealthSection(driver).size(), 0, "Menu Items are Blank");

			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}

	}

	@Test(description = "verify Fashion section", priority = 7)
	public void verifyFashionSection() throws Exception {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getFashionSection(driver).size());
			for (WebElement e : homepageobjct.getFashionSection(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getFashionSection(driver).size(), 0, "Menu Items are Blank");

			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}

	}

	@Test(description = "verify grooming section", priority = 7)
	public void verifyGroomingSection() throws Exception {

		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getGroomingSection(driver).size());
			for (WebElement e : homepageobjct.getGroomingSection(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getGroomingSection(driver).size(), 0, "Menu Items are Blank");

			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}

	}

	@Test(description = "Verify RHS Ad 1", priority = 5)
	public void verifyRhsAd1() throws Exception {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		if (homepageobjct.getRhsAd1(driver).isDisplayed()) {
			System.out.println("Adframe is Visible");
		} else {

			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
			System.out.println("Element is InVisible");
		}

	}

	@Test(description = "verify Entertainment Section", priority = 7)
	public void verifyAroundTheWeb() throws Exception {

		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getRHSAroundtheWeb(driver).size());
			for (WebElement e : homepageobjct.getRHSAroundtheWeb(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getRHSAroundtheWeb(driver).size(), 0, "Around the web are Blank");
			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
		}

	}

	@Test(description = "verify Latest Section", priority = 7)
	public void verifyLatestSection() throws Exception {

		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			System.out.println(homepageobjct.getRHSLatest(driver).size());
			for (WebElement e : homepageobjct.getRHSLatest(driver)) {
				System.out.println(e.getText());
				Assert.assertNotEquals(homepageobjct.getRHSLatest(driver).size(), 0, "Around the web are Blank");
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}
		} catch (Exception e) {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
			
			
		}

	}

	@Test(description = "verify RHS Ad2", priority = 3)

	public void verifyRhsAd2() throws Exception {

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		if (homepageobjct.getRHSAd2(driver).isDisplayed()) {
			System.out.println("Adframe2 is Visible");
		} else {

			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("D:\\testScreenShot.jpg"));
			System.out.println("Element is InVisible");
		}
	}
	@AfterSuite()
	public void sendEmail() throws Exception{
		System.out.println("sending email............");
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		SendEmail.execute(reportFileName,SuiteName);
	
	}


}
