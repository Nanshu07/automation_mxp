package TestCases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import BaseTest.BaseClass;
import DataProviders.ServerUrls;
import Page_Methods.HttpConnectionClass;

public class ServerCheck extends BaseClass {

	BaseClass b2;
	//String apUrl="http://amp.mensxp.com/healthcheck";
	
	HttpConnectionClass con;
	@BeforeClass(alwaysRun = true)
	public void alwayssetup() {
		b2 = new BaseClass();
		con=new HttpConnectionClass();
	}
	
	@Test (dataProvider = "ServerUrls", dataProviderClass = ServerUrls.class)
	public void CheckResponseCode(String Url) throws Exception{
		try{
		con.isLinkBroken(Url);
	}
		catch(IOException e) {
			System.out.println("Failed while checking for ==> " + Url);
			e.printStackTrace();
		}
		
}
	
}


	