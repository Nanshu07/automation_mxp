package DataProviders;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;

public class ServerUrls {
	@DataProvider(name="ServerUrls")
	
	public static Object[][] getDataFromDataprovider(Method m){
		 if (m.getName().equalsIgnoreCase("CheckResponseCode"))
		 {
        return new Object[][] {

                {"http://amp.mensxp.com/healthcheck"  },
                {"http://www.mensxp.com/healthcheck.php"},
                {"https://notify.mensxp.com/healthcheck.php"},
                {"http://192.169.35.195/healthcheck"},
                {"http://192.169.35.194/healthcheck"},
                {"http://192.169.31.205/healthcheck.php"},
                {"http://192.169.31.206/healthcheck.php"},
                {"http://192.169.31.207/healthcheck.php"},
                {"http://192.169.31.208/healthcheck.php"},
                {"http://192.169.31.209/healthcheck.php"}
                
               
            };

		 }
		
		 else
		{
			
		return new Object[][]{
			
		
            {"http://www.mensxp.com/healthcheck.php"}
           
			
		};
			
			
		}

    }}