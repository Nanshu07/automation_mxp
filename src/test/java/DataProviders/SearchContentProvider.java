package DataProviders;

import java.lang.reflect.Method;

import org.testng.annotations.DataProvider;

public class SearchContentProvider {
@DataProvider(name="SearchContent")
	
	public static Object[][] getDataFromDataprovider(Method m){
		 if (m.getName().equalsIgnoreCase("VerifySearchAPI"))
		 {
        return new Object[][] {

                { "http://www.mensxp.com/contentsearch.php?q=fashion" },
                { "http://www.mensxp.com/contentsearch.php?q=Style" },
                { "http://www.mensxp.com/contentsearch.php?q=samsung" }
                
               
            };

		 }
		
		 else
		{
			
		return new Object[][]{
			
			{"http://www.mensxp.com/contentsearch.php?q=fashion"}
			
		};
			
			
		}

    }
}
