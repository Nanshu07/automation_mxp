package Utilities;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;


import java.util.*;

public class SendEmail

{
//reportFileName = TestExecutionResultFileName
public static void execute(String reportFileName, String SuiteName) throws Exception

{
String path="C:\\Selenium_Automation\\eclipse\\Workspace\\MensXP_Automation\\target\\Test-Output\\emailable-report.html";

String[] to={"nanshu.arora@timesinternet.in"};
String[] cc={/*"Manu.Fotedar@timesinternet.in,Ramji.Tripathi@timesinternet.in"*/};
String[] bcc={"nanshu1607@gmail.com"};

SendEmail.sendMail("nanshutimes2016@gmail.com",
"Naina@1991",
"smtp.gmail.com",
"465",
"true",
"true",
true,
"javax.net.ssl.SSLSocketFactory",
"false",
to,
cc,
bcc,
"Automation Report",
"Please find attached Automation Report",
path,
reportFileName,SuiteName);
}

public static boolean sendMail(String userName,
String passWord,
String host,
String port,
String starttls,
String auth,
boolean debug,
String socketFactoryClass,
String fallback,
String[] to,
String[] cc,
String[] bcc,
String subject,
String text,
String attachmentPath,
String attachmentName,
String SuiteName){

//Object Instantiation of a properties file.
Properties props = new Properties();

props.put("mail.smtp.user", userName);


props.put("mail.smtp.host", host);

if(!"".equals(port)){
props.put("mail.smtp.port", port);
}

if(!"".equals(starttls)){
props.put("mail.smtp.starttls.enable",starttls);
props.put("mail.smtp.auth", auth);
}

if(debug){

props.put("mail.smtp.debug", "true");

}else{

props.put("mail.smtp.debug", "false");

}

if(!"".equals(port)){
props.put("mail.smtp.socketFactory.port", port);
}
if(!"".equals(socketFactoryClass)){
props.put("mail.smtp.socketFactory.class",socketFactoryClass);
}
if(!"".equals(fallback)){
props.put("mail.smtp.socketFactory.fallback", fallback);
}

try{

Session session = Session.getDefaultInstance(props, null);

session.setDebug(debug);

MimeMessage msg = new MimeMessage(session);



msg.setSubject(subject+"for"+SuiteName+" | "+new Date());



Multipart multipart = new MimeMultipart();
MimeBodyPart messageBodyPart = new MimeBodyPart();

DataSource source = new FileDataSource(attachmentPath);
messageBodyPart.setDataHandler(new DataHandler(source));
messageBodyPart.setFileName(attachmentName);
multipart.addBodyPart(messageBodyPart);
System.out.println("file attached");

msg.setContent(multipart);
msg.setFrom(new InternetAddress(userName));

for(int i=0;i<to.length;i++){
msg.addRecipient(Message.RecipientType.TO, new
InternetAddress(to[i]));
}

for(int i=0;i<cc.length;i++){
msg.addRecipient(Message.RecipientType.CC, new
InternetAddress(cc[i]));
}

for(int i=0;i<bcc.length;i++){
msg.addRecipient(Message.RecipientType.BCC, new
InternetAddress(bcc[i]));
}

msg.saveChanges();

Transport transport = session.getTransport("smtp");

transport.connect(host, userName, passWord);

transport.sendMessage(msg, msg.getAllRecipients());

transport.close();

return true;

} catch (Exception mex){
mex.printStackTrace();
return false;
}
}
}