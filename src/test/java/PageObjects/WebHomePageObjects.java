package PageObjects;

import java.util.List;


import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebHomePageObjects {

	public WebDriver driver;

	public String expectedTitle = "MensXP.com - India's largest Online lifestyle magazine for Men. Offering tips & advice on relationships, fashion, office, health & grooming";

	public String actualTitle = null;

	public String getACtualTitle(WebDriver driver) {
		actualTitle = driver.getTitle();
		return actualTitle;

	}

	public By logo = By.xpath(".//*[@id='layout']/header/div/a[1]/span");

	public By menuItems = By.cssSelector(".NavLinks.ga-block.ga-entity");

	public By Banners = By.cssSelector(".thumbs>li");

	public By arrow = By.cssSelector(".nxt>span");

	public By WhtsHot = By.cssSelector("#section_WhatsHot");

	public By TrndngCom = By.id("sr0000");

	public By gallery = By.cssSelector(".gallerySection.MainBlock.blk.ga-block");

	public By techSection = By.cssSelector("Block pod blk");

	public By relationShip = By.cssSelector(".relationship.MainBlock.blk.ga-block");

	public By healthSection = By.cssSelector(".health.MainBlock.blk.ga-block");

	public By fashionection = By.cssSelector(".fashion.MainBlock.blk.ga-block");

	public By groomingSection = By.cssSelector(".grooming.MainBlock.blk.ga-block");

	public By WorkLifeSection = By.cssSelector(".worklife.MainBlock.blk.ga-block");

	public By adIframe = By.id("div-gpt-ad-1397419575494-1");

	public By RhsAd = By.id("div-gpt-ad-1397419575494-0");

	public By RhsExpand = By.cssSelector("#expand-button");

	public By RhsClose = By.cssSelector("#close-button");

	public By RhsLatest = By.cssSelector("#mustRead.StoryListing.mustRead");

	public By RhsAroundDWeb = By.id("mensxp_hp_rcmw_rhs_ctn_nat");

	public By RhsAd2 = By.id("div-gpt-ad-1397419575494-2");
	
	public By allLinks=By.tagName("a");
	
	public By headHealth1=By.xpath("html/body/div[4]/header/div/nav/a[2]/span");

	
	public By headHealth=By.cssSelector("#megaMenu");
	
	
	public By HealthSubmenus=By.xpath("html/body/div[4]/div[1]/div/div[1]/div[1]/div");
	public By HeadRelatnship=By.xpath("html/body/div[4]/header/div/nav/a[3]/span");
	
	public By relatnshipMenu=By.xpath("//div[contains(@class,'menu relationship blk ga-block')]");
	
	public By Headerfashion=By.xpath("html/body/div[4]/header/div/nav/a[4]/span");
	
	public By fashionMenu=By.xpath("//div[contains(@class,'menu fashion blk ga-block')]");
	
	public By HeaderGrooming=By.xpath("html/body/div[4]/header/div/nav/a[5]/span");
	public By GroomingMenu=By.xpath("//div[contains(@class,'menu grooming blk ga-block')]");
	
	public By HeaderWorkLife=By.xpath("html/body/div[4]/header/div/nav/a[6]/span");
	public By WorkLifeMenu=By.xpath("//div[contains(@class,'menu worklife blk ga-block')]");
	
	public By HeaderTechnology=By.xpath("html/body/div[4]/header/div/nav/a[7]/span");
	public By TechnologyMenu=By.xpath("//div[contains(@class,'menu technology blk ga-block')]");
	
	public By HeaderEntertainment=By.xpath("html/body/div[4]/header/div/nav/a[8]/span");
	public By EntertainmentMenu=By.xpath("//div[contains(@class,'menu entertainment blk ga-block')]");
	

	public WebElement getlogo(WebDriver driver) {
		return driver.findElement(logo);
	}

	public List<WebElement> getmenuItems(WebDriver driver) {

		return driver.findElements(menuItems);
	}

	public List<WebElement> getBannerData(WebDriver driver) {
		return driver.findElements(Banners);
	}

	public WebElement getArrow(WebDriver driver) {

		return driver.findElement(arrow);
	}

	public List<WebElement> getWhtsHotsection(WebDriver driver) {
		return driver.findElements(WhtsHot);
	}

	public List<WebElement> getTrendingCom(WebDriver driver) {
		return driver.findElements(TrndngCom);
	}

	public List<WebElement> getGallery(WebDriver driver) {
		return driver.findElements(gallery);
	}

	public List<WebElement> getTechSection(WebDriver driver) {
		return driver.findElements(techSection);
	}

	public List<WebElement> getRelationSection(WebDriver driver) {
		return driver.findElements(relationShip);
	}

	public List<WebElement> getHealthSection(WebDriver driver) {
		return driver.findElements(healthSection);
	}

	public List<WebElement> getFashionSection(WebDriver driver) {
		return driver.findElements(fashionection);
	}

	public List<WebElement> getGroomingSection(WebDriver driver) {
		return driver.findElements(groomingSection);
	}

	public List<WebElement> getWorkSection(WebDriver driver) {
		return driver.findElements(WorkLifeSection);
	}

	public WebElement getAdFrame(WebDriver driver) {
		return driver.findElement(adIframe);
	}

	public WebElement getRhsAd1(WebDriver driver) {
		return driver.findElement(RhsAd);
	}

	public WebElement getRHSCloseButton(WebDriver driver) {
		return driver.findElement(RhsClose);
	}

	public List<WebElement> getRHSLatest(WebDriver driver) {
		return driver.findElements(RhsLatest);
	}

	public List<WebElement> getRHSAroundtheWeb(WebDriver driver) {
		return driver.findElements(RhsAroundDWeb);
	}

	public WebElement getRHSAd2(WebDriver driver) {
		return driver.findElement(RhsAd2);
	}
	
	public List<WebElement> getAllLinks(WebDriver driver){
		
		return driver.findElements(allLinks);
	}
	

public WebElement getHeaderHealth1(WebDriver driver){
		
		return driver.findElement(headHealth1);
	}
	public List<WebElement> getHeaderHealth(WebDriver driver){
		
		return driver.findElements(headHealth);
	}
	public List<WebElement> getHealthSubmenu(WebDriver driver){
		
		return driver.findElements(HealthSubmenus);
	}
	
	public WebElement getheaderRelatnshp(WebDriver driver){
		return driver.findElement(HeadRelatnship);
	}
	
public List<WebElement> getRElationshipmenu(WebDriver driver){
		
		return driver.findElements(relatnshipMenu);
	}
public WebElement getFashionHeader(WebDriver driver){
	return driver.findElement(Headerfashion);
}
public List<WebElement> getFashionMenu(WebDriver driver){
	
	return driver.findElements(fashionMenu);
} 
public WebElement getGroomingHeader(WebDriver driver){
	return driver.findElement(HeaderGrooming);
}
public List<WebElement> getGroomingMenu(WebDriver driver){
	
	return driver.findElements(groomingSection);
} 

public WebElement getWorkLifeHeader(WebDriver driver){
	return driver.findElement(HeaderWorkLife);
}
public List<WebElement> getWorkLifeMenu(WebDriver driver){
	
	return driver.findElements(WorkLifeMenu);
} 
public WebElement getTechnologyHeader(WebDriver driver){
	return driver.findElement(HeaderTechnology);
}
public List<WebElement> getTechnologyMenu(WebDriver driver){
	
	return driver.findElements(TechnologyMenu);
} 
public WebElement getEntertainmentHeader(WebDriver driver){
	return driver.findElement(HeaderEntertainment);
}
public List<WebElement> getEntertainmentMenu(WebDriver driver){
	
	return driver.findElements(EntertainmentMenu);
} 

}
