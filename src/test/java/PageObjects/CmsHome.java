package PageObjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;


public class CmsHome {
	
	protected WebDriver driver;
	
	
	@FindBy(linkText="content")
    private WebElement menuItem;
	
	@FindBy(linkText="original content")
	private WebElement subMenuItem;
	
	@FindBy()
	private String contentPageTitle="Original Content Management";
/*	private String actualContentPageTitle=driver.getTitle();
	private By addNew= By.cssSelector("#addcontent");	
	*/
	public CmsHome(WebDriver driver) {
		this.driver=driver;
	}
  
	
	public WebElement getMenuItem() {
		return menuItem;
	}
	
	public WebElement getSubMenuItem() {
		return subMenuItem;
	}
}


