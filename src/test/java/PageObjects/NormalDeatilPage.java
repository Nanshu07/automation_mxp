package PageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NormalDeatilPage {
	public WebDriver driver;

	public By BreadcrupSet = By.cssSelector("#breadCrumbsSet");

	public By BreadcrumpList = By.cssSelector("breadCrumb blk entertainment");

	public By socialLhs = By.cssSelector(".social_inner");

	public By socialId = By.id("sc_link");

	public By clickFbIcon = By.cssSelector(".art_fb.art_icon");

	public By clickTwitrIcon = By.cssSelector(".art_fb.art_icon");
	public By clickGooplusIcoon = By.cssSelector(".art_fb.art_icon");
	public By clickReditIcon = By.cssSelector(".art_fb.art_icon");
	public By clickMailIcon = By.cssSelector(".art_fb.art_icon");

	public By TopImage = By.cssSelector(".inWrap>img");

	public By TopHeading = By.cssSelector(".caption>h1");

	public By AuthorStrip = By.cssSelector(".publish_info.clearfix");

	public By AuthorInfor = By.cssSelector(".author_info");

	public By AuthorThumbimg = By.cssSelector(".author_thumb>img");

	public By AuthorName = By.cssSelector(".author_det>span>a");

	public By PublishedDate = By.xpath("html/body/div[4]/div[5]/article/div/div[3]/div[1]/div[1]/div[1]/p/span[2]");

	public By TopSocialbar = By.cssSelector("#social_data");

	public By Fb_count = By.cssSelector("#u_0_2");

	public By Fb_like = By.cssSelector("#u_0_3");

	public By TopSubCat = By.cssSelector(".followers.Bg");

	public By LinkedinShare = By.cssSelector("#li_ui_li_gen_1481267579753_0-title");

	public By TwitrShare = By.id("b");

	public By redditShare = By.cssSelector(".reddit>img");

	public By aroundMore = By.cssSelector(".OpduSjVSat");

	public By AroundTheWeb = By.xpath("html/body/div[4]/div[5]/article/div/div[3]/div[1]/article/div[2]/div[1]");
	
	public By MrFromMensXp=By.cssSelector(".NUdqcef");
	
	

	public WebElement getBreadCrump(WebDriver driver) {
		return driver.findElement(BreadcrupSet);
	}

	public List<WebElement> getBreadcrumpList(WebDriver driver) {
		return driver.findElements(BreadcrumpList);
	}

	public WebElement getSocialLhs(WebDriver driver) {
		return driver.findElement(socialLhs);
	}

	public WebElement getSocialId(WebDriver driver) {
		return driver.findElement(socialId);
	}

	public WebElement getTopHeading(WebDriver driver) {
		return driver.findElement(TopHeading);
	}

}
