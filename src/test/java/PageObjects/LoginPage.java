package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class LoginPage {
	
	protected WebDriver driver;
	
	@FindBy(id="username")
	private WebElement username;
	
	@FindBy(id="password")
	private WebElement pwd;
	
	@FindBy(css=".btnLogin")
	private WebElement loginBtn;
	
	
	public WebElement getUsername() {
		return username;
	}

	public WebElement getPassword() {
		return pwd;
	}
	public WebElement getloginBtn() {
		return loginBtn;
	}
	
}
