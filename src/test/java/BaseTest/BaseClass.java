package BaseTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.remote.RemoteWebDriver;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;




public class BaseClass {

	public static WebDriver driver;
	static String driverPath = "D:\\chromedriver\";";
	String reportFileName="emailable-report.html";
	
	public WebDriver getDriver() {
		return driver;

	}
public BaseClass(){}
	public void setDriver(String browserType, String appUrl) {
		switch (browserType) {
		case "chrome":

			driver = initChromeDriver(appUrl);
			break;
		case "FireFox":
			driver = initFirefoxDriver(appUrl);
			break;

		}
	}

	private static WebDriver initChromeDriver(String appURL) {
		String driverPath="C:\\Selenium_Automation\\chromedriver.exe";
		System.out.println("Launching google chrome with new profile..");
		System.setProperty("webdriver.chrome.driver",driverPath);
		RemoteWebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.navigate().to(appURL);
		return driver;
	}

	private static WebDriver initFirefoxDriver( String appURL) {
		System.out.println("Launching Firefox browser..");
		
		
		//FirefoxProfile ffp = new FirefoxProfile(); 
		//ffp.setPreference("general.useragent.override","Mozilla/5.0 (Windows NT x.y; Win64; x64; rv:10.0) Gecko/20100101 Firefox/10.0");
		RemoteWebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.navigate().to(appURL);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		return driver;
	}
	@Parameters({ "browserType", "appURL" })
	@BeforeClass
	public void initializeTestBaseSetup(@Optional("browserType") String browserType,@Optional("appUrl")String appUrl) {
		try {
			
			setDriver(browserType, appUrl);

		} catch (Exception e) {
			System.out.println("Error....." + e.getStackTrace());
		}
	}
	
	@AfterSuite(alwaysRun = true)
public void tearDown() throws Exception {
		if (driver != null)
			driver.quit();
			/*driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			SendEmail.execute(reportFileName);*/
		}
}

	
	
	

